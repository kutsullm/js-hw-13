const imgWrapper = document.querySelector(".images-wrapper")
const start= document.querySelector("#start")
const stop= document.querySelector("#stop")
let timer = 3000
let currentInd = 0
let interval = createInterval()
function goToNextImage(){
    imgWrapper.children[currentInd].hidden = true
    currentInd++
    if(currentInd === imgWrapper.children.length){
        currentInd = 0
    }
      imgWrapper.children[currentInd].hidden = false     
}
function createInterval(){
    return setInterval(() =>{
        timer -= 10
        if(timer <= 0 ){
            timer = 3000
            goToNextImage()
        }
        const timerParent = document.querySelector("#timer")
        timerParent.textContent = (timer/1000).toFixed(3)
    },10)
}
stop.addEventListener("click",toStop)
function toStop(event){
clearInterval(interval)
}
start.addEventListener("click",toStart)
function toStart(event){
clearInterval(interval)
interval=createInterval()
}